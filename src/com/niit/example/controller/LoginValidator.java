package com.niit.example.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.niit.example.dao.UserDAO;

/**
 * Servlet implementation class LoginValidator
 */
@WebServlet(description = "Login validation controller", urlPatterns = { "/login" })
public class LoginValidator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserDAO userDAO;
	private RequestDispatcher dispatcher;
	public LoginValidator() {
		userDAO=new UserDAO();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		if(userDAO.validate(username, password)) {
			dispatcher=request.getRequestDispatcher("WEB-INF/views/Home.jsp");
			dispatcher.forward(request, response);
		}
		else {
			dispatcher=request.getRequestDispatcher("WEB-INF/views/Login.jsp");
			dispatcher.forward(request, response);
		}
		
	}

}
