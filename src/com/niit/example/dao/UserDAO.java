package com.niit.example.dao;

import java.util.LinkedList;
import java.util.List;

import com.niit.example.model.User;

public class UserDAO {

	private List<User> users;
	
	public UserDAO() {
		users = new LinkedList<User>();
		users.add(new User("john","password"));
		users.add(new User("kenny","pass"));
		users.add(new User("billy","word"));
	}
	
	public boolean validate(String username,String password) {
		
		if(users.contains(new User(username,password)))
			return true;
		else
			return false;
	}
	
	
	
}
